# {Day} {DayNumber}

### :sunrise: At the beginning of the working day
ToDos: # 
MRs: # 
Issues: # 
Email: # 


### What would you like to accomplish today?

1. [ ] Async standup update.
1. [ ] 
1. [ ] 


### :night_with_stars: At the end of the working day


#### :bulb: What you learned today / What were the WOW moments / etc


### :ballot_box_with_check:  What are the new action items (to add them to the backlog)

1. [ ] 
1. [ ] 
1. [ ]
