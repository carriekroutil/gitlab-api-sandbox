<!-- 
Instructions
[ ] Title this issue as WXX YYY-MM-DD
[ ] Update availabiitliy
[ ] Add goals

Ledgend
{Month}          -> Replaced with current month. e.g. January
{MMM}            -> Replaced with first three letters of month. e.g. JAN
{WeekDayDates}   -> Replaced with the current week day's dates. e.g. JAN 16 - JAN 20
-->

# {Month} :icon: | {DescriptionWeekDates}

**What**
* :one: xyz
* :two: xyz
* :three: xyz

**How**
1. xyz
    1. [ ] xyz
1. xyz
    1. [ ] xyz
1. xyz
    1. [ ] xyz

**What I would like to do, but may not be able to do...**

1. [ ] xyz

/assign @carriekroutil
/due next Monday
/label ~week-journal
